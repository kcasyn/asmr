# Attack Surface Mapping and Recon

External network enumeration tool written in Go.

## Required External Tools

These are external go tools required for all functionality. They can be installed with the `asmr install` command.

- amass: <https://github.com/OWASP/Amass>
- gobuster: <https://github.com/OJ/gobuster>
- httpx: <https://github.com/projectdiscovery/httpx>
- subfinder: <https://github.com/projectdiscovery/subfinder>
- tlsx: <https://github.com/projectdiscovery/tlsx>
- gowitness: <https://github.com/sensepost/gowitness>
- naabu: <https://github.com/projectdiscovery/naabu>
- ffuf: <https://github.com/ffuf/ffuf>

## Usage

This is the basic usage for now. This is very much in alpha stage, so is subject to change. Do not run with sudo unless you have copied the go tool binaries to a location in path.

```bash
Usage: asmr [[install]] [[network|domain|web|all] -hf HostFile -od OutDir [-dnsw DNSWordlist|-dirw DIRWordlist]]

Subcommands:
    install         Installs external go tools needed

    network         Performs ping and port discovery scans
    domain          Performs DNS and (sub)domain enumeration (requires -dnsw Wordlist)
    web             Performs web server enumeration (requires -dirw Wordlist)
    all             Performs all checks (requires -dnsw and -dirw Wordlists)

Flags:
    -hf             File containing a list of line seperated targets. Can be IPs, domains, or both
    -od             Directory to store output files
    -dnsw           Subdomain bruteforce wordlist
    -dirw           Directory bruteforce wordlist
```

## TODO

Tasks:

- optimization
- convert external tools to modules if possible

Tool Implementation:

Functionality:

- Parse out discovered IP/domains/sub-domains
- ADS Discovery (Azure)
- email/user enumeration
- recursive domain/subdomain enumeration
- validate new domains/subdomains resolve to inscope ips

## Completed

Tool Implementation

- amass
- gowitness
- ctfr
- gobuster
- nmap
- subfinder
- gau
- ffuf

Functionality

- DNS Recon
- DNSSEC check
- nmap scans (ping,TCP)
- RDNS
- SMTP checks: Identify presence/absence of SPF/DMARC/DKIM
- Sub domain enumeration (amass, subfinder, gobuster)
- web scraping (gowitness)
- ADS Discovery (NTLM,Lync,Autodiscover)
- SSL Cert scanning
- concurrency
- everything writes to files
- directory bruteforcing

## References

References for external tools

- amass: <https://github.com/OWASP/Amass>
- gobuster: <https://github.com/OJ/gobuster>
- httpx: <https://github.com/projectdiscovery/httpx>
- subfinder: <https://github.com/projectdiscovery/subfinder>
- tlsx: <https://github.com/projectdiscovery/tlsx>
- gowitness: <https://github.com/sensepost/gowitness>
- naabu: <https://github.com/projectdiscovery/naabu/>
- gau: <https://github.com/lc/gau>
- ffuf: <https://github.com/ffuf/ffuf>
