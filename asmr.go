package main

import (
	"bytes"
	"crypto/tls"
	"encoding/binary"
	"encoding/csv"
	"encoding/gob"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"

	"github.com/miekg/dns"
	"github.com/mpvl/unique"
	"github.com/peterzen/goresolver"
	"golang.org/x/net/publicsuffix"
)

//----------HELPERS----------

// funtion to check and print errors
func check(e error, s string) {
	if e != nil {
		log.Printf("[!] ERROR stage: %s error: %s\n", s, e)
	}
}

//----------TYPES----------

// data struct for ctfr
type data struct {
	IssuerCaID        int    `json:"issuer_ca_id"`
	IssuerName        string `json:"issuer_name"`
	NameValue         string `json:"name_value"`
	MinCertID         int    `json:"min_cert_id"`
	MinEntryTimestamp string `json:"min_entry_timestamp"`
	NotAfter          string `json:"not_after"`
	NotBefore         string `json:"not_before"`
}

//----------TOOLS----------

// perform ping and port scans
func naabuScan(ips []string, domains []string, outDir string) {
	log.Println("[+] Performing asset discovery")

	// create target list
	targets := append(ips, domains...)
	targetList := strings.Join(targets, ",")
	var alive []string

	log.Println("[+] Performing ping scan")

	// naabu -silent -sn
	cmdPing, errPing := exec.Command("sudo", "naabu", "-silent", "-host", targetList, "-sn", "-o", filepath.Join(outDir, "ping_scan.txt")).Output()
	check(errPing, "ping scan")
	fmt.Println(string(cmdPing))

	// store results
	pingResults := strings.Split(string(cmdPing), "\n")
	for _, host := range pingResults {
		split := strings.Split(host, ":")
		alive = append(alive, split[0])
	}

	log.Println("[+] Completed ping scan")

	log.Println("[+] Performing TCP discovery scan")

	// naabu -silent -p <ports>
	cmdTCPDisco, errTCPDisco := exec.Command("naabu", "-silent", "-host", targetList, "-p", "21,22,23,25,53,79,80,81,110,139,143,443,445,465,514,993,1433,1521,2902,3389,3306,5432,5800,5900,8000,8080,8300,8433,8500,8501,8888,9090,9100,10000,51010", "-o", filepath.Join(outDir, "tcp_disco_scan.txt")).Output()
	check(errTCPDisco, "tcp disco")
	fmt.Println(string(cmdTCPDisco))

	// store results
	tcpDiscoResults := strings.Split(string(cmdTCPDisco), "\n")
	for _, host := range tcpDiscoResults {
		split := strings.Split(host, ":")
		alive = append(alive, split[0])
	}

	log.Println("[+] Completed TCP discovery scan")

	// sort and unique alive hosts
	unique.Sort(unique.StringSlice{P: &alive})
	unique.Strings(&alive)

	// write alive hosts to file
	f, err := os.Create(filepath.Join(outDir, "alive.txt"))
	check(err, "alive host create file")
	defer f.Close()

	for _, ip := range alive {
		_, err := f.WriteString(ip + "\n")
		check(err, "alive host write file")
	}

	f.Sync()

	// amke alive list
	aliveList := strings.Join(alive, ",")

	log.Println("[+] Performing full TCP scan on live assets")

	// naabu -silent -p -
	cmdTCPFull, errTCPFull := exec.Command("naabu", "-silent", "-host", aliveList, "-p", "-", "-o", filepath.Join(outDir, "tcp_full_scan.txt")).Output()
	check(errTCPFull, "tcp full scan")
	fmt.Println(string(cmdTCPFull))

	log.Println("[+] Completed full TCP scan of live assets")

	log.Println("[+] Completed asset discovery")
}

// perform a reverse DNS lookup on provided IPs
func rDNS(ips []string, outDir string) {
	log.Println("[+] Performing RDNS lookups")

	// create and open file to write data to
	f, err := os.Create(filepath.Join(outDir, "rdns.txt"))
	check(err, "rdns file create")
	defer f.Close()

	// perform a lookup on each ip
	for _, ip := range ips {
		domains, err := net.LookupAddr(ip)

		if err == nil && len(domains) > 0 {
			for _, domain := range domains {
				txt := ip + ":" + domain
				fmt.Println(txt)
				_, err := f.WriteString(txt + "\n")
				check(err, "rdns file write")
			}
		} else {
			fmt.Println(ip + ":No records")
		}
	}
	f.Sync()

	log.Println("[+] Completed RDNS lookups")
}

// perform various DNS record lookups and checks
func dnsRecon(domains []string, outDir string) {
	log.Println("[+] Performing DNS recon")

	dnsOutDir := filepath.Join(outDir, "dns")
	os.Mkdir(dnsOutDir, 0777)
	resolverFile := filepath.Join(dnsOutDir, "resolver.txt")
	_, errExist := os.Stat(resolverFile)
	if os.IsNotExist(errExist) {
		r := []byte("nameserver 1.1.1.1")
		errCreate := os.WriteFile(resolverFile, r, 0777)
		check(errCreate, "resolver file create")
	}

	for _, domain := range domains {
		log.Printf("[+] Performing DNS recon on %s\n", domain)
		// create and open file to write data to
		f, err := os.Create(filepath.Join(dnsOutDir, domain+".txt"))
		check(err, "dnsrecon file create")
		defer f.Close()

		// check DNSSEC
		resolver, err := goresolver.NewResolver(resolverFile)
		check(err, "dnsrecon resolver create")
		dnssec, err := resolver.StrictNSQuery(dns.Fqdn(domain), dns.TypeA)
		if err != nil {
			fmt.Printf("DNSSEC is not enabled for %s\n", domain)
			_, err := f.WriteString("DNSSEC is not enabled for " + domain + "\n")
			check(err, "DNSSEC file write")
		} else {
			fmt.Printf("DNSSEC is enabled for %s: %s\n", domain, dnssec)
			_, err := f.WriteString("DNSSEC is enabled for " + domain + "\n")
			check(err, "DNSSEC file write")
		}
		// check TXT records
		txts, err := net.LookupTXT(domain)
		if err == nil && len(txts) > 0 {
			for _, txt := range txts {
				if strings.Contains(txt, "spf") {
					fmt.Println("SPF:" + txt)
					_, err := f.WriteString(txt + "\n")
					check(err, "TXT record file write")
				} else {
					fmt.Println("TXT:" + txt)
					_, err := f.WriteString(txt + "\n")
					check(err, "TXT record file write")
				}
			}
		} else {
			fmt.Println("TXT:No records")
		}

		// check DMARC records
		dmarcs, err := net.LookupTXT("_dmarc." + domain)
		if err == nil && len(dmarcs) > 0 {
			for _, dmarc := range dmarcs {
				fmt.Println("DMARC:" + dmarc)
				_, err := f.WriteString(dmarc + "\n")
				check(err, "DMARC record file write")
			}
		} else {
			fmt.Println("DMARC:No records")
		}

		// check CNAME records
		cname, err := net.LookupCNAME(domain)
		if err == nil && len(cname) > 0 {
			fmt.Println("CNAME:" + cname)
			_, err := f.WriteString(cname + "\n")
			check(err, "CNAME record file write")
		} else {
			fmt.Println("CNAME:No records")
		}

		// check MX records
		mxs, err := net.LookupMX(domain)
		if err == nil && len(mxs) > 0 {
			for _, mx := range mxs {
				fmt.Println("MX:" + mx.Host)
				_, err := f.WriteString(mx.Host + "\n")
				check(err, "MX record file write")
			}
		} else {
			fmt.Println("MX:No records")
		}

		// check NS records
		nss, err := net.LookupNS(domain)
		if err == nil && len(nss) > 0 {
			for _, ns := range nss {
				fmt.Println("NS:" + ns.Host)
				_, err := f.WriteString(ns.Host + "\n")
				check(err, "NS record file write")
			}
		} else {
			fmt.Println("NS:No records")
		}

		// perform an AXFR request
		if err == nil && len(nss) > 0 {
			for _, s := range nss {
				tr := dns.Transfer{}
				m := new(dns.Msg)
				m.SetAxfr(dns.Fqdn(domain))
				in, err := tr.In(m, s.Host+":53")
				if err == nil && len(in) != 0 {
					for ex := range in {
						for _, a := range ex.RR {
							var record, ip, hostname string
							switch v := a.(type) {
							case *dns.A:
								record = "A:"
								ip = v.A.String()
								hostname = v.Hdr.Name
							case *dns.AAAA:
								record = "AAAA:"
								ip = v.AAAA.String()
								hostname = v.Hdr.Name
							case *dns.PTR:
								record = "PTR:"
								ip = v.Hdr.Name
								hostname = v.Ptr
							case *dns.NS:
								cip, err := net.LookupAddr(v.Ns)
								if err != nil || len(cip) == 0 {
									continue
								}
								record = "NS:"
								ip = cip[0]
								hostname = v.Ns
							case *dns.CNAME:
								cip, err := net.LookupAddr(v.Target)
								if err != nil || len(cip) == 0 {
									continue
								}
								record = "CNAME:"
								ip = cip[0]
								hostname = v.Hdr.Name
							case *dns.SRV:
								cip, err := net.LookupAddr(v.Target)
								if err != nil || len(cip) == 0 {
									continue
								}
								record = "SRV:"
								ip = cip[0]
								hostname = v.Target
							default:
								continue
							}
							txt := record + ip + strings.TrimRight(hostname, ".")
							fmt.Println(txt)
							_, err := f.WriteString(txt + "\n")
							check(err, "AXFR record file write")
						}
					}
				} else {
					fmt.Println("DNS Zone Transfer failed")
				}
			}
		} else {
			fmt.Println("DNS Zone Transfer failed")
		}
		f.Sync()
	}

	log.Println("[+] Completed DNS recon")
}

func ctfr(domains []string, outDir string) {
	log.Println("[+] Performing CTFR lookups")

	// initialize variables
	var keys []data

	// retrieve results from crt.sh
	for _, domain := range domains {
		pubSuffixDomain, _ := publicsuffix.EffectiveTLDPlusOne(domain)
		checkDomain := "%." + pubSuffixDomain
		resp, err := http.Get(fmt.Sprintf("https://crt.sh/?q=%s&output=json", checkDomain))
		if err, ok := err.(*url.Error); ok {
			if err.Timeout() {
				log.Println("request timed out")
			} else if err.Temporary() {
				log.Println("temporary error")
			} else {
				log.Printf("%s\n", err.Err)
			}
		}
		if resp.StatusCode != 200 {
			log.Printf("unexpected status code returned: %d\n", resp.StatusCode)
		}
		body, err := io.ReadAll(resp.Body)
		check(err, "crt.sh lookup")

		json.Unmarshal([]byte(body), &keys)
	}

	// write results if any to csv
	counter := make(map[string]int)
	file, err := os.Create(filepath.Join(outDir, "ctfr.csv"))
	check(err, "ctfr file write")
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, i := range keys {
		counter[i.NameValue]++
		if counter[i.NameValue] == 1 {
			var tmp = []string{i.NameValue}
			err := writer.Write(tmp)
			check(err, "ctfr file write")
		}
	}

	log.Println("[+] Completed CTFR lookups")
}

func ads(ips []string, domains []string, outDir string) {
	log.Println("[+] Performing ADS scans")

	targetList := strings.Join(domains, ",")
	adsOutDir := filepath.Join(outDir, "ads")
	os.Mkdir(adsOutDir, 0777)

	for _, domain := range domains {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}

		req, err := http.NewRequest("HEAD", "https://lyncdiscover."+domain, nil)
		check(err, "lyncdiscover lookup")

		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		} else {
			defer resp.Body.Close()

			file, err := os.Create(filepath.Join(adsOutDir, domain+".lyncdiscover.txt"))
			check(err, "lyncdiscover file write")
			defer file.Close()

			r := fmt.Sprintf("%s %d %s\n", resp.Proto, resp.StatusCode, http.StatusText(resp.StatusCode))
			_, err1 := file.WriteString(r)
			check(err1, "lyncdiscover file write")
			for k, v := range resp.Header {
				var value string
				for _, x := range v {
					value = value + " " + x
				}
				h := k + ":" + value + "\n"
				_, err := file.WriteString(h)
				check(err, "lyncdiscover file write")
			}
			file.Sync()
		}
	}

	for _, domain := range domains {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client := &http.Client{Transport: tr}

		req, err := http.NewRequest("HEAD", "https://autodiscover."+domain, nil)
		check(err, "autodiscover lookup")
		req.SetBasicAuth("user", "password")

		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
		} else {
			defer resp.Body.Close()

			file, err := os.Create(filepath.Join(adsOutDir, domain+".autodiscover.txt"))
			check(err, "autodiscover file write")
			defer file.Close()

			r := fmt.Sprintf("%s %d %s\n", resp.Proto, resp.StatusCode, http.StatusText(resp.StatusCode))
			_, err1 := file.WriteString(r)
			check(err1, "autodiscover file write")
			for k, v := range resp.Header {
				var value string
				for _, x := range v {
					value = value + " " + x
				}
				h := k + ":" + value + "\n"
				_, err := file.WriteString(h)
				check(err, "autodiscover file write")
			}
			file.Sync()
		}
	}

	// nmap ntlm script via naabu
	cmdNTLM, errNTLM := exec.Command("naabu", "-silent", "-host", targetList, "-p", "80,443", "-nmap-cli", "\"nmap --script http-ntlm-info -oA "+filepath.Join(adsOutDir, "ads-ntm-http")+"\"", "-o", filepath.Join(adsOutDir, "ping_scan.txt")).Output()
	check(errNTLM, "NTLM info")
	fmt.Println(string(cmdNTLM))

	log.Println("[+] Completed ADS scans")
}

//----------WRAPPERS----------

// wrappers for tools that are too difficult to re-implement (at least until I figure out an easy way to add them)
// these are standalone go binaries but they are only able to be installed on linux
// make sure go is installed and run the install command

func subEnum(domains []string, outDir string, wordlist string) {
	log.Println("[+] Performing (sub)domain enumeration")

	//ipList := strings.Join(ips, ",")
	domainList := strings.Join(domains, ",")

	log.Println("[+] Performing amass scans")
	// create amass output directory
	amassOutDir := filepath.Join(outDir, "amass")
	os.Mkdir(amassOutDir, 0777)
	// perform both an enum scan with amass on each domain
	cmdEnum, errEnum := exec.Command("amass", "enum", "-active", "-brute", "-w", wordlist, "-d", domainList, "-o", filepath.Join(amassOutDir, "enum.txt")).Output()
	check(errEnum, "amass cmd")
	fmt.Println(string(cmdEnum))
	log.Println("[+] Completed amass scans")

	log.Println("[+] Performing subdomain bruteforcing")
	// make gobuster output directory
	dnsbruteOutDir := filepath.Join(outDir, "dnsbrute")
	os.Mkdir(dnsbruteOutDir, 0777)
	// perform scans
	for _, domain := range domains {
		cmdDnsbrute, errDnsbrute := exec.Command("gobuster", "dns", "-i", "-t", "50", "-d", domain, "-w", wordlist, "-o", filepath.Join(dnsbruteOutDir, domain+".txt")).Output()
		check(errDnsbrute, "gobuster cmd")
		fmt.Println(string(cmdDnsbrute))
	}
	log.Println("[+] Completed subdomain bruteforcing")

	log.Println("[+] Performing subfinder scans")
	// make subfinder output directory
	subfinderOutDir := filepath.Join(outDir, "subfinder")
	os.Mkdir(subfinderOutDir, 0777)
	// run scans
	cmdSubfinder, errSubfinder := exec.Command("subfinder", "-silent", "-d", domainList, "-o", filepath.Join(subfinderOutDir, "subfinder.txt")).Output()
	check(errSubfinder, "subfinder cmd")
	fmt.Println(string(cmdSubfinder))
	log.Println("[+] Completed subfinder scans")

	log.Println("[+] Completed enumeration of (sub)domains")
}

func webscrape(ips []string, domains []string, outDir string) {
	log.Println("[+] Performing web scraping")

	// create a list of ips and domains seperated by commas
	targets := append(ips, domains...)
	targetList := strings.Join(targets, ",")
	// create directory for report
	gowitnessOutDir := filepath.Join(outDir, "gowitness")
	errDir := os.Mkdir(gowitnessOutDir, 0777)
	check(errDir, "gowitness file create")
	// set output location for db and screenshots
	dbOutput := filepath.Join(gowitnessOutDir, "gowitness.sqlite3")
	ssOutput := filepath.Join(gowitnessOutDir, "screenshots")
	// create the commands
	// use naabu to perform a quick port scan
	cmdNaabu := exec.Command("naabu", "-silent", "-host", targetList, "-p", "80-89,440-449,4080-4089,4440-4449,8080-8089,8440-8449,8840-8849,8880-8889,9080-9089,9440-9449")
	// check open ports for web servers
	cmdHttpx := exec.Command("httpx", "-silent")
	// take screenshots of web servers
	cmdGowitness := exec.Command("gowitness", "file", "-f", "-", "-D", dbOutput, "-P", ssOutput)
	// create read and write pipes
	hr, nw := io.Pipe()
	cmdNaabu.Stdout = nw
	cmdHttpx.Stdin = hr
	gr, hw := io.Pipe()
	cmdHttpx.Stdout = hw
	cmdGowitness.Stdin = gr
	// create a buffer to hold output
	var b bytes.Buffer
	cmdGowitness.Stdout = &b
	// start each command
	cmdNaabu.Start()
	cmdHttpx.Start()
	cmdGowitness.Start()
	// wait on naabu command to write to nw
	log.Println("[+] Discovering services...")
	cmdNaabu.Wait()
	// close the writer to pass the output to httpx
	nw.Close()
	// wait on httpx command to write to hw
	log.Println("[+] Checking for web servers...")
	cmdHttpx.Wait()
	// close the writer to pass the output to gowitness
	hw.Close()
	// wait on gowitness to write to buffer
	log.Println("[+] Taking screenshots...")
	cmdGowitness.Wait()
	// print gowitness output to stdout
	io.Copy(os.Stdout, &b)

	// generate gowitness report
	cmdReport := exec.Command("gowitness", "report", "export", "-f", filepath.Join(gowitnessOutDir, "webscrape.zip"), "-D", dbOutput, "-P", ssOutput)
	errReport := cmdReport.Run()
	check(errReport, "gowitness report create")

	log.Println("[+] Completed web scraping")
}

func sslCertScan(domains []string, outDir string) {
	log.Println("[+] Performing SSL certificate scanning")

	tlsxOutFile := filepath.Join(outDir, "sslcertscan.txt")
	domainList := strings.Join(domains, ",")
	cmdTlsx, errTlsx := exec.Command("tlsx", "-silent", "-u", domainList, "-cn", "-san", "-o", tlsxOutFile).Output()
	check(errTlsx, "tlsx cmd")
	fmt.Println(string(cmdTlsx))

	log.Println("[+] Completed SSL certificate scanning")
}

func gauScan(domains []string, outDir string) {
	log.Println("[+] Performing URL scanning")

	gauOutFile := filepath.Join(outDir, "urlscan.txt")
	domainList := strings.Join(domains, " ")
	cmdGau, errGau := exec.Command("gau", "--fp", "--subs", "--threads", "10", "--o", gauOutFile, domainList).Output()
	check(errGau, "gau cmd")
	fmt.Println(string(cmdGau))

	log.Println("[+] Completed URL scanning")
}

func dirbrute(domains []string, wordlist string, outDir string) {
	log.Println("[+] Performing directory bruteforcing")

	// make gobuster output directory
	dirbruteOutDir := filepath.Join(outDir, "dirbrute")
	os.Mkdir(dirbruteOutDir, 0777)
	buf := &bytes.Buffer{}
	gob.NewEncoder(buf).Encode(domains)
	inputFile := "domains.txt"
	err := os.WriteFile(inputFile, []byte(buf.Bytes()), 0777)
	check(err, "dirbrute file create")
	defer os.RemoveAll(inputFile)
	urlsraw, urlsrawErr := exec.Command("httpx", "-silent", "-l", inputFile, "-t", "100").Output()
	check(urlsrawErr, "httpx cmd")
	urls := strings.Split(string(urlsraw), "\n")
	// perform scans
	for _, target := range urls[:len(urls)-1] {
		domain, _ := url.Parse(target)
		cmdDirbrute, errDirbrute := exec.Command("ffuf", "-u", target+"/FUZZ", "-w", wordlist, "--recursion", "-o", filepath.Join(dirbruteOutDir, domain.Hostname()+".json")).Output()
		check(errDirbrute, "ffuf cmd")
		fmt.Println(string(cmdDirbrute))
	}

	log.Println("[+] Completed directory bruteforcing")
}

//----------PARSING----------

// Convert CIDRs to an IP list
func convertCIDR(netw string) []string {
	// convert string to IPNet struct
	_, ipv4Net, _ := net.ParseCIDR(netw)

	// convert IPNet struct mask and address to uint32
	mask := binary.BigEndian.Uint32(ipv4Net.Mask)
	// find the start IP address
	start := binary.BigEndian.Uint32(ipv4Net.IP)
	// find the final IP address
	finish := (start & mask) | (mask ^ 0xffffffff)
	// make a slice to return host addresses
	var hosts []string
	// loop through addresses as uint32.
	for i := start; i <= finish; i++ {
		// convert back to net.IPs
		// Create IP address of type net.IP. IPv4 is 4 bytes, IPv6 is 16 bytes.
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		hosts = append(hosts, ip.String())
	}
	// return a slice of strings containing IP addresses
	return hosts
}

// Create IP list scope
func createIpList(hostFileText string) []string {
	// Regex for IPs
	ipRegex, _ := regexp.Compile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$`)
	// Regex for CIDRs
	cidrRegex, _ := regexp.Compile(`(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}/[1-3]{0,1}[0-9]`)
	// Parse our CIDR ranges, expand to single IPs, and add to IP list
	cidrs := cidrRegex.FindAllString(hostFileText, -1)
	var ips []string
	for _, cidr := range cidrs {
		cidrList := convertCIDR(cidr)
		ips = append(ips, cidrList...)
	}
	// Add any other IPs to the IP list
	tempips := ipRegex.FindAllString(hostFileText, -1)
	ips = append(ips, tempips...)
	// Sort and unique the IP list
	unique.Sort(unique.StringSlice{P: &ips})
	unique.Strings(&ips)
	// Return the IP list
	return ips
}

// Create Domain list for scope
func createDomainList(hostFileText string) []string {
	// Regex for domains
	domainRegex, _ := regexp.Compile(`.*\.([a-z]+)`)
	// Create domain list
	domains := domainRegex.FindAllString(hostFileText, -1)
	// Return domain list
	return domains
}

// Get IPs from domain names
func getDomainIP(domains []string) []string {
	var domainIps []string
	for _, domain := range domains {
		ips, _ := net.LookupIP(domain)
		for _, ip := range ips {
			if ipv4 := ip.To4(); ipv4 != nil {
				domainIps = append(domainIps, ip.String())
			}
		}
	}
	return domainIps
}

func hostFileParse(hostFile string) string {
	// Parse host file
	hostFileContent, err := os.ReadFile(hostFile)
	if err != nil {
		log.Println("Invalid host file")
		os.Exit(1)
	}
	hostFileText := string(hostFileContent)
	return hostFileText
}

func targetLists(targetList string) ([]string, []string) {
	// Generate IP and Domain lists
	ips := createIpList(targetList)
	domains := createDomainList(targetList)
	domainIPs := getDomainIP(domains)
	// Add domain IPs to master IP list
	ips = append(ips, domainIPs...)
	unique.Sort(unique.StringSlice{P: &ips})
	unique.Strings(&ips)
	return ips, domains
}

//--------INSTALL---------

// Installs external Go tools
func install() {
	// install amass
	installAmass, errAmass := exec.Command("go", "install", "-v", "github.com/OWASP/Amass/v3/...@master").Output()
	check(errAmass, "amass install")
	fmt.Println(string(installAmass))

	// install gobuster
	installGobuster, errGobuster := exec.Command("go", "install", "-v", "github.com/OJ/gobuster/v3@latest").Output()
	check(errGobuster, "gobuster install")
	fmt.Println(string(installGobuster))

	// install httpx
	installHttpx, errHttpx := exec.Command("go", "install", "-v", "github.com/projectdiscovery/httpx/cmd/httpx@latest").Output()
	check(errHttpx, "httpx install")
	fmt.Println(string(installHttpx))

	// install tlsx
	installTlsx, errTlsx := exec.Command("go", "install", "-v", "github.com/projectdiscovery/tlsx/cmd/tlsx@latest").Output()
	check(errTlsx, "tlsx install")
	fmt.Println(string(installTlsx))

	// install subfinder
	installSubfinder, errSubfinder := exec.Command("go", "install", "-v", "github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest").Output()
	check(errSubfinder, "subfinder install")
	fmt.Println(string(installSubfinder))

	// install gowitness
	installGowitness, errGowitness := exec.Command("go", "install", "-v", "github.com/projectdiscovery/tlsx/cmd/tlsx@latest").Output()
	check(errGowitness, "gowitness install")
	fmt.Println(string(installGowitness))

	// install naabu
	installNaabu, errNaabu := exec.Command("go", "install", "-v", "github.com/projectdiscovery/naabu/v2/cmd/naabu@latest").Output()
	check(errNaabu, "naabu install")
	fmt.Println(string(installNaabu))

	// install gau
	installGau, errGau := exec.Command("go", "install", "-v", "github.com/lc/gau/v2/cmd/gau@latest").Output()
	check(errGau, "gau install")
	fmt.Println(string(installGau))

	// install ffuf
	installFfuf, errFfuf := exec.Command("go", "install", "-v", "github.com/ffuf/ffuf@latest").Output()
	check(errFfuf, "ffuf install")
	fmt.Println(string(installFfuf))
}

//----------MAIN----------

func main() {
	usage := `ASMR is a tool written in go to help automate external attack surface recon and data gathering.

Usage: asmr [[install]] [[network|domain|web|all] -hf HostFile -od OutDir [-dnsw DNSWordlist|-dirw DIRWordlist]]

Subcommands:
	install         Installs external go tools needed

	network         Performs ping and port discovery scans
	domain          Performs DNS and (sub)domain enumeration (requires -dnsw Wordlist)
	web             Performs web server enumeration (requires -dirw Wordlist)
	all             Performs all checks (requires -dnsw and -dirw Wordlists)

Flags:
	-hf             File containing a list of line seperated targets. Can be IPs, domains, or both
	-od             Directory to store output files
	-dnsw           Subdomain bruteforce wordlist
	-dirw           Directory bruteforce wordlist
	`

	naabuFlag := flag.NewFlagSet("nmap", flag.ExitOnError)
	naabuHost := naabuFlag.String("hf", "hosts.txt", "Host list to target")
	naabuOut := naabuFlag.String("od", ".", "Output directory")

	domainFlag := flag.NewFlagSet("rdns", flag.ExitOnError)
	domainHost := domainFlag.String("hf", "hosts.txt", "Host list to target")
	domainOut := domainFlag.String("od", ".", "Output directory")
	domainWordlist := domainFlag.String("dnsw", "/usr/share/seclists/Discovery/DNS/subdomains-top1million-20000.txt", "Wordlist for bruteforcing subdomains")

	webFlag := flag.NewFlagSet("webscrape", flag.ExitOnError)
	webHost := webFlag.String("hf", "hosts.txt", "Host list to target")
	webOut := webFlag.String("od", ".", "Output directory")
	webWordlist := webFlag.String("dirw", "/usr/share/seclists/Discovery/Web-Content/common.txt", "Wordlist for directory bruteforcing")

	allFlag := flag.NewFlagSet("all", flag.ExitOnError)
	allHost := allFlag.String("hf", "hosts.txt", "Host list to target")
	allOut := allFlag.String("od", ".", "Output directory")
	alldnsbruteWordlist := allFlag.String("dnsw", "/usr/share/seclists/Discovery/DNS/subdomains-top1million-20000.txt", "Wordlist for bruteforcing subdomains")
	alldirbruteWordlist := allFlag.String("dirw", "/usr/share/seclists/Discovery/Web-Content/common.txt", "Wordlist for directory bruteforcing")

	// Check for number of args and print usage if not valid
	if len(os.Args) < 2 {
		fmt.Println(usage)
		os.Exit(1)
	}

	// Parse flags
	switch os.Args[1] {
	case "network":
		// parse flags
		naabuFlag.Parse(os.Args[2:])
		ips, domains := targetLists(hostFileParse(*naabuHost))
		outDir := filepath.Join(*naabuOut, "assets")
		os.Mkdir(outDir, 0777)
		// get sudo for ping scan
		cmdsudo, errsudo := exec.Command("sudo", "echo", "sudo success!").Output()
		check(errsudo, "get sudo")
		fmt.Println(string(cmdsudo))
		naabuScan(ips, domains, outDir)
	case "domain":
		// parse flags
		domainFlag.Parse(os.Args[2:])
		ips, domains := targetLists(hostFileParse(*domainHost))
		wordlist := *domainWordlist
		outDir := filepath.Join(*domainOut, "domains")
		os.Mkdir(outDir, 0777)
		// create wait group
		var wg sync.WaitGroup
		wg.Add(6)
		go func() {
			defer wg.Done()
			rDNS(ips, outDir)
		}()
		go func() {
			defer wg.Done()
			dnsRecon(domains, outDir)
		}()
		go func() {
			defer wg.Done()
			ctfr(domains, outDir)
		}()
		go func() {
			defer wg.Done()
			subEnum(domains, outDir, wordlist)
		}()
		go func() {
			defer wg.Done()
			ads(ips, domains, outDir)
		}()
		go func() {
			defer wg.Done()
			sslCertScan(domains, outDir)
		}()
		wg.Wait()
	case "web":
		webFlag.Parse(os.Args[2:])
		ips, domains := targetLists(hostFileParse(*webHost))
		outDir := filepath.Join(*webOut, "web")
		os.Mkdir(outDir, 0777)
		wordlist := *webWordlist
		// create wait group
		var wg sync.WaitGroup
		wg.Add(3)
		go func() {
			defer wg.Done()
			gauScan(domains, outDir)
		}()
		go func() {
			defer wg.Done()
			webscrape(ips, domains, outDir)
		}()
		go func() {
			defer wg.Done()
			dirbrute(domains, wordlist, outDir)
		}()
		wg.Wait()
	case "all":
		log.Println("[+] Starting all scanners")
		// parse flags
		allFlag.Parse(os.Args[2:])
		ips, domains := targetLists(hostFileParse(*allHost))
		assetsOutDir := filepath.Join(*allOut, "assets")
		domainOutDir := filepath.Join(*allOut, "domain")
		webOutDir := filepath.Join(*allOut, "web")
		dnswordlist := *alldnsbruteWordlist
		dirwordlist := *alldirbruteWordlist
		os.Mkdir(assetsOutDir, 0777)
		os.Mkdir(domainOutDir, 0777)
		os.Mkdir(webOutDir, 0777)
		// create wait group
		var wg sync.WaitGroup
		wg.Add(7)
		// get sudo for ping scan
		cmdsudo, errsudo := exec.Command("sudo", "echo", "sudo success!").Output()
		check(errsudo, "get sudo")
		fmt.Println(string(cmdsudo))
		go func() {
			defer wg.Done()
			naabuScan(ips, domains, assetsOutDir)
		}()
		go func() {
			defer wg.Done()
			rDNS(ips, domainOutDir)
		}()
		go func() {
			defer wg.Done()
			dnsRecon(domains, domainOutDir)
		}()
		go func() {
			defer wg.Done()
			ctfr(domains, domainOutDir)
		}()
		go func() {
			defer wg.Done()
			subEnum(domains, domainOutDir, dnswordlist)
		}()
		go func() {
			defer wg.Done()
			ads(ips, domains, domainOutDir)
		}()
		go func() {
			defer wg.Done()
			sslCertScan(domains, domainOutDir)
		}()
		go func() {
			defer wg.Done()
			gauScan(domains, webOutDir)
		}()
		wg.Wait()
		webscrape(ips, domains, webOutDir)
		dirbrute(domains, dirwordlist, webOutDir)
		log.Println("[+] All scanners complete")
	case "install":
		install()
	default:
		fmt.Println(usage)
		os.Exit(1)
	}
}
