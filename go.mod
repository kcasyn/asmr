module github.com/kcasyn/asmr

go 1.19

require (
	github.com/Ullaakut/nmap/v2 v2.2.1
	github.com/miekg/dns v1.1.50
	github.com/mpvl/unique v0.0.0-20150818121801-cbe035fff7de
	github.com/peterzen/goresolver v1.0.2
	golang.org/x/net v0.0.0-20220927171203-f486391704dc
)

require (
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	golang.org/x/tools v0.1.6-0.20210726203631-07bc1bf47fb2 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
